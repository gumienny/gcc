Extension of [official gcc image](https://hub.docker.com/_/gcc/) tailored for cmake c/c++ projects.
Latest image version available at [Docker Hub](https://hub.docker.com/r/gumienny/gcc/).
